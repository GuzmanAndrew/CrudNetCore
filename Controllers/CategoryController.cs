﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrudNet.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CrudNet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        [HttpGet]
        public ActionResult Get()
        {
            using (Models.BlogDBContext db = new Models.BlogDBContext())
            {
                var lst = (from d in db.Persona
                           select new Persona
                           {
                               IdPersona = d.IdPersona,
                               LastNamePersona = d.LastNamePersona,
                               NamePersona = d.NamePersona
                           }
                           ).ToList();
                return Ok(lst);

            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] Models.Request.CategoryRequest model)
        {
            String Mensaje = "";
            try
            {
                using (Models.BlogDBContext db = new Models.BlogDBContext())
                {
                    Models.Persona oCategory = new Models.Persona
                    {
                        NamePersona = model.Nombres,
                        LastNamePersona = model.Apellidos
                    };
                    db.Persona.Add(oCategory);
                    db.SaveChanges();
                    Mensaje = "Se guardo Correctamente, con Id: " + oCategory.IdPersona;
                }
            }
            catch (Exception ex)
            {
                Mensaje = ex.Message;
            }
            return Ok(Mensaje);

        }

        [HttpPut]
        public ActionResult Put([FromBody] Models.Request.PersonaEditRequest model)
        {
            String Mensaje = "";
            try
            {
                using (Models.BlogDBContext db = new Models.BlogDBContext())
                {
                    Models.Persona oPersona = db.Persona.Where(x => x.IdPersona == model.Id).FirstOrDefault();
                    if (oPersona != null)
                    {
                        oPersona.NamePersona = model.Nombres;
                        oPersona.LastNamePersona = model.Apellidos;
                        db.Entry(oPersona).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                        db.SaveChanges();
                        Mensaje = "Se actualizó correctamente la categoria, id = " + model.Id;
                    }
                    else
                    {
                        Mensaje = "No se encontraron resultados para la categoria, Id =" + model.Id;
                    }
                }
            }
            catch (Exception ex)
            {
                Mensaje = ex.Message;
            }
            return Ok(Mensaje);
        }

        [HttpDelete]
        public ActionResult Delete([FromBody] Models.Request.PersonaEditRequest model)
        {
            using (Models.BlogDBContext db = new Models.BlogDBContext())
            {
                Models.Persona oPersona = db.Persona.Find(model.Id);
                db.Persona.Remove(oPersona);
                db.SaveChanges();
            }

            return Ok();
        }
    }
}