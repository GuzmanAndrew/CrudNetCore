﻿using System;
using System.Collections.Generic;

namespace CrudNet.Models
{
    public partial class Persona
    {
        public int IdPersona { get; set; }
        public string NamePersona { get; set; }
        public string LastNamePersona { get; set; }
    }
}
